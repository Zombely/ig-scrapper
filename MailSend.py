import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import Loader

data = Loader.json_loader()
sender_email = data["AEM_mail"]
sender_pass = data["sender_pass"]
port = 465
smtp_server = data["smtp_server"]


class Sender():
    def __init__(self, dst_mail, link,order_id,customer_name):

        self.receiver_address = dst_mail
        self.link = '"'+link+'"'
        self.order_id = order_id
        self.customer_name = customer_name
        self.sender_email = sender_email
        message = MIMEMultipart("alternative")
        message["Subject"] = "Instagram Sheet"
        message["From"] = self.sender_email
        message["To"] = self.receiver_address

        # Write the plain text part
        text = """\
        Hi"""+self.customer_name+""",
        Check out your Instagram Sheet from 2thePoint.
        Your order Id:"""+self.order_id+""" 
        Instagram Sheet """+self.link+""" Enjoy!"""
        #text += """In case of missing link """+self.link

        # write the HTML part
        html = """\
        <html>
            <body>
                <p>Hi """+self.customer_name+""",<br>
                    Check out your Instagram Sheet from 2thePoint. Your order Id:"""+self.order_id+"""</p> 
                        <p>
                            <a href="""+self.link+""">Instagram Sheet</a>
                        Enjoy!</p> 
            </body>
        </html> 
        """

        # convert both parts to MIMEText objects and add them to the MIMEMultipart message

        part1 = MIMEText(text, "plain")
        part2 = MIMEText(html, "html")
        message.attach(part1)
        message.attach(part2)

        # send your email
        with smtplib.SMTP_SSL(smtp_server, port) as server:
            server.login(sender_email, sender_pass)
            server.sendmail(sender_email, self.receiver_address, message.as_string())
            server.quit()

