from Sheets import SheetsModule
from MailSend import Sender
from UsersInfo import UserInfo
from DataClass import InstaRequests
import json
import Loader


def do_all_operations(info_tab):

    data = Loader.json_loader()
    tag = info_tab[0]
    mail = info_tab[1]
    name = f'Instagram Sheet: {tag}'
    profile_count = int(info_tab[2])
    like_count = int(info_tab[3])
    followers_count = int(info_tab[4])
    AEM_mail = data["AEM_mail"]
    order_id = "TEST"
    customer_name = info_tab[5]

    sheet_tab = []

    def get_user_info(username_list):

        for i in range(len(username_list)):
            a = UserInfo(username_list[i])
            if a.followers >= followers_count and len(sheet_tab) < profile_count:
                sheet_tab.append(a.profile_info)
        if len(sheet_tab) < profile_count:
            username_list_next = insta_object.get_username(insta_object.get_post_id(insta_object.top_media_request_next()))
            get_user_info(username_list_next)
        else:     
            return sheet_tab


    insta_object = InstaRequests(tag, profile_count, like_count)
    hashtag_id = insta_object.hashtag_request()
    top_media = insta_object.topmedia_request(hashtag_id)

    username_l = insta_object.get_username(insta_object.get_post_id(top_media))
    username_list_clear = list(dict.fromkeys(username_l))
    print(len(username_list_clear))
    #if len(username_list_clear) >= profile_count:
    get_user_info(username_list_clear)
    sheet_object = SheetsModule(mail, name)
    sheet_object.add_to_sheet(sheet_tab)

        #mail to customer
    Sender(mail, sheet_object.spreadsheet_url,order_id,customer_name)
    Sender(AEM_mail,sheet_object.spreadsheet_url,order_id,customer_name)
        #save order in file and mail to us     
    #else:
        #incoming fix
        #print('profiles missing')
    output_info = []
    output_info.append(sheet_object.spreadsheet_url)
    output_info.append(mail)

    return output_info
    
def json_loader():
    f = open('Info.json','r')
    data = json.loads(f.read())
    return data

