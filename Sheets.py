import gspread as gc
from oauth2client.service_account import ServiceAccountCredentials
import pandas as pd
from df2gspread import df2gspread as d2g
import Loader



scope = ["https://spreadsheets.google.com/feeds", 'https://www.googleapis.com/auth/spreadsheets',"https://www.googleapis.com/auth/drive.file","https://www.googleapis.com/auth/drive"]

creds = ServiceAccountCredentials.from_json_keyfile_name("E:\Scrapper\Creds.json", scope)
data = Loader.json_loader()
client = gc.authorize(creds)
AEM_mail = data["AEM_mail"]
grid = [['Profile name', 'Profile Link', 'Followers', 'Posts']]


class SheetsModule:
    def __init__(self, dst_mail, sheet_name):
        self.mail = dst_mail
        self.name = sheet_name
        self.new_sheet = client.create(self.name)
        if 'gmail' in self.mail:
            self.new_sheet.share(self.mail, perm_type='user', role='writer', notify=False)
        else:
            self.new_sheet.share(self.mail, perm_type='user', role='writer', notify=True)
        self.spreadsheet_url = "https://docs.google.com/spreadsheets/d/%s" % self.new_sheet.id+"/edit?usp=sharing"
        if self.mail != AEM_mail:
            self.new_sheet.share(AEM_mail, perm_type='user', role='owner', notify=False)

    def add_to_sheet(self, list_added):
        sheet_list = grid + list_added
        df = pd.DataFrame(data = sheet_list)
        d2g.upload(df,gfile = self.new_sheet.id,wks_name = self.name,credentials = creds,row_names=False,col_names=False)
        worksheet_list = self.new_sheet.worksheets()
        self.new_sheet.del_worksheet(worksheet_list[0])

