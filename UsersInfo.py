import requests
import json
import Loader

data = Loader.json_loader()
ACCESS_TOKEN = data["ACCESS_TOKEN"]
PAGE_ID = data["PAGE_ID"]
IG_ID = data["IG_ID"]


class UserInfo:
    def __init__(self, username):
        self.username = username
        self.profile_info = []
        self.ig_link = f'https://www.instagram.com/{self.username}/'
        connection_string = 'https://graph.facebook.com/v6.0/'+IG_ID+'?fields=business_discovery.username('+self.username+'){followers_count,media_count}&access_token='+ACCESS_TOKEN
        user_info = requests.get(connection_string).text
        user_info_loaded = json.loads(user_info)
        try:
            print(user_info_loaded)
            self.followers = user_info_loaded["business_discovery"]["followers_count"]
            self.posts = user_info_loaded["business_discovery"]["media_count"]
        except Exception:
            print(user_info_loaded)
            self.followers = 0
            self.posts = 0
        self.profile_info.append(self.username)
        self.profile_info.append(self.ig_link)
        self.profile_info.append(self.followers)
        self.profile_info.append(self.posts)
