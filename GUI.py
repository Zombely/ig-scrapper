import tkinter as tk
from Calculations import do_all_operations


def create_gui():
    window = tk.Tk()
    label_names = ["Tag", "Mail", "Profile Count", "Like Count", "Followers Count", "Customer Name"]
    window.title("Instagram Scrapper")
    input_tab = []
    label_tab = []
    


    for i in range(len(label_names)):
        label_tab.append(tk.Label(window, text=f"{label_names[i]}:").grid(row = i))
    
    input_box_tab = []

    for i in range(len(label_names)):
        input_box_tab.append(tk.Entry(window, width = 50))
        input_box_tab[i].grid(row = i, column = 1)


    def get_info():
        if input_tab == []:
            for i in range(len(label_names)):
                input_tab.append(input_box_tab[i].get())
            if '' in input_tab:
                error_window = tk.Tk()
                error_window.title("Error")
                error_window.geometry("200x50")
                tk.Label(error_window, text="Fill all blanks").pack()
                input_tab.clear()
            if (len(input_tab) == len(label_names)):
                
                
                s = do_all_operations(input_tab)
                for i in range(len(s)):
                    text_box.insert(tk.END, s[i])
               

    
    tk.Button(window, text = "Accept", command = get_info, width = 5).grid(row = len(label_names)+1,column = 0)
    text_box = tk.Text(window, height = 5, width = 60)
    tk.Label(window, text="Output").grid(row = len(label_names) + 2 ,columnspan = 2)
    text_box.grid(row = len(label_names) + 3, columnspan = 2)
        
    
    tk.mainloop()
    
    


create_gui()