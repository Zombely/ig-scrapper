import requests
import json
import Loader


data = Loader.json_loader()
ACCESS_TOKEN = data["ACCESS_TOKEN"]
PAGE_ID = data["PAGE_ID"]
IG_ID = data["IG_ID"]


class InstaRequests:
    def __init__(self, hashtag_name, profile_count, like_count):

        self.hashtag_name = hashtag_name
        self.profile_count = profile_count
        self.like_count = like_count

    def hashtag_request(self):
        try:
            hash_connection_str = f"https://graph.facebook.com/v6.0/ig_hashtag_search?user_id={IG_ID}&q={self.hashtag_name}&access_token={ACCESS_TOKEN}"
            hashtag_id = requests.get(hash_connection_str).text
            hashtag_loaded = json.loads(hashtag_id)
            self.hashtag_string = hashtag_loaded["data"][0]["id"]
            return self.hashtag_string
        except KeyError:
            print(hashtag_loaded)
            print(KeyError)
            print("Access token invalid")
            print("Get new one - https://developers.facebook.com/tools/explorer/") 


    def topmedia_request(self, hashtag_string):
        media_connection_str = f"https://graph.facebook.com/v6.0/{hashtag_string}/top_media?user_id={IG_ID}&fields=comments_count,like_count,permalink&access_token={ACCESS_TOKEN}"
        instagram_urls = []
        while len(instagram_urls) < self.profile_count:
            media_request = requests.get(media_connection_str).text
            self.media_loaded = json.loads(media_request)

            for i in range(len(self.media_loaded["data"])):
                if int(self.media_loaded["data"][i]["like_count"]) >= self.like_count:
                    instagram_urls.append(self.media_loaded["data"][i]["permalink"])

            if len(instagram_urls) < self.profile_count:
                media_connection_str = self.media_loaded["paging"]["next"]

        return instagram_urls

    def get_post_id(self, instagram_url):

        instagram_post_id = []
        for i in range(len(instagram_url)):
            a = instagram_url[i]
            instagram_post_id.append(a.split('/')[4])
        return instagram_post_id

    def get_username(self, posts_id):

        usernames = []
        for i in range(len(posts_id)):
            username_query = f'https://www.instagram.com/graphql/query/?query_hash=6ff3f5c474a240353993056428fb851e&variables=%7B%22shortcode%22%3A%22{posts_id[i]}%22%2C%22include_reel%22%3Atrue%2C%22include_logged_out%22%3Afalse%7D'
            try:
                a = requests.get(username_query).text
                a2 = json.loads(a)
                usernames.append(a2["data"]["shortcode_media"]["owner"]["reel"]["owner"]["username"])
            except Exception as limit_error:
                print(limit_error)
                print(a2)
                print("get_username")
        return usernames

    def top_media_request_next(self):
        instagram_urls_next = []
        media_connection_str_next = self.media_loaded["paging"]["next"]
        media_request_next = requests.get(media_connection_str_next).text
        media_next_loaded = json.loads(media_request_next)

        for i in range(len(media_next_loaded["data"])):
            if int(media_next_loaded["data"][i]["like_count"]) >= self.like_count:
                instagram_urls_next.append(media_next_loaded["data"][i]["permalink"])
        return instagram_urls_next


